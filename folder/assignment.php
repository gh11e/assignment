<?php
//require('person.php');

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        
        <h1>XYZ Organization</h1> 
       
        <table border="1" width="60%">
                
        
        <?php
        
        $multArray = array();
        $multArray[] = array('fname' => 'Tom', 'lname' => 'Green', 'title' => 'Computer Systems Control Specialist', 'office' => '123 DSL', 'Phone' => '(850) 555.1422', 'email' => '<a href="mailto:t.green@example.xyz.org">t.green@example.org</a>');
        $multArray[] = array('fname' => 'Xiaoguang', 'lname' => 'Li', 'title' => 'Systems Administrator', 'office' => '456 DSL', 'Phone' => '(850) 555.0188', 'email' => '<a href="mailto:x.li@example.xyz.org">x.li@example.xyz</a>');
        $multArray[] = array('fname' => 'Edson', 'lname' => 'Manners', 'title' => 'Applications Adminstrator', 'office' => '789 DSL', 'Phone' => '(850) 555.6490', 'email' => '<a href="mailto:e.manners@example.xyz.org">e.manners@example.xyz.org</a>');
        $multArray[] = array('fname' => 'Justin', 'lname' => 'Marshall', 'title' => 'Systems Adminstrator', 'office' => '987 DSL', 'Phone' => '(850) 554.5559', 'email' => '<a href="mailto:j.marshall@example.xyz.org">j.marshall@example.xyz.org</a>');
        $multArray[] = array('fname' => 'Michael', 'lname' => 'McDonald', 'title' => 'Assistant in Research / Systems Adminstrator', 'office' => '654 DSL', 'Phone' => '(850) 577.7032', 'email' => '<a href="mailto:m.mcdonald@example.xyz.org">m.mcdonald@example.xyz.org</a>');
        $multArray[] = array('fname' => 'Donny', 'lname' => 'Shrum', 'title' => 'SSERCA.org', 'office' => '321 DSL', 'Phone' => '(850) 588.7061', 'email' => '<a href="mailto:d.shrum@example.xyz.org">d.shrum@example.xyz.org</a>');
        $multArray[] = array('fname' => 'Dan', 'lname' => 'Voss', 'title' => 'HPC Specialist', 'office' => '159 DSL', 'Phone' => '(850) 511.0179', 'email' => '<a href="mailto:d.voss@example.xyz.org">d.voxx@example.xyz.org</a>');
        $multArray[] = array('fname' => 'Jim', 'lname' => 'Wilgenbusch', 'title' => 'Research Associate / TSG Manager', 'office' => '951 DSL', 'Phone' => '(850) 522.0307', 'email' => '<a href="mailto:j.wilgenbusch@example.xyz.org">j.wilgenbusch@example.xyz.org</a>');
        $multArray[] = array('fname' => 'Paul', 'lname' => 'van der Mark', 'title' => 'Assistant in Research / HPC Operational Manager', 'office' => '248 DSL', 'Phone' => '(850) 533.0193', 'email' => '<a href="mailto:p.vandermark@example.xyz.org">p.vandermark@example.xyz.org</a>')
        ;
        
        // HTML validator has a hidden <td></td> in the multArray which gives me an error
            foreach($multArray as $person){
                echo "<tr><td>" . $person['fname'] . " " . $person['lname'] . "</td><td>" . $person['title'] . "</td><td> " . $person['office'] . "</td><td>" . $person['Phone'] . "</td><td>" . $person["email"] . "</tr></td>";
            }
           
        ?>
        </table>
    </body>
        
</html>
