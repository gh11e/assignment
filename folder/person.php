<?php

class Person
{
    //Here we create two different variables: one for first name
    //and another for last name.  This will make retrieval easier
    private $firstName;
    private $lastName;

    private $title;
    private $office;
    private $phone;
    private $email;


    /**
     * Constructor
     *
     * The name is kept as a single string, so we only accept one
     * parameter for both first and last name (you probably did it
     * differently by accepting two parameters, which is fine)
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    // --------------------------------------------------------------

    /**
     * Get the name
     *
     * Our application expects the name to be in 'firstName lastName'
     * format, so we will concatenate here.
     *
     * @return string
     */
    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    // --------------------------------------------------------------

    /**
     * Set the name
     *
     * Here, we will use the explode() string function to break the
     * name up into first and lastname for storage.
     *
     * @param string $name
     */
    public function setName($name)
    {
        //Break up the name using explode() -- See php.net
        list($lname, $fname) = explode(',', $name, 2);

        //Set the class properties.  The firstName and lastName
        //will have extra spaces after running explode(), so trim them
        $this->firstName = trim($fname);
        $this->lastName  = trim($lname);
    }


    // --------------------------------------------------------------

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    // --------------------------------------------------------------

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    // --------------------------------------------------------------

    /**
     * Get office
     *
     * @return string
     */
    public function getOffice()
    {
        return $this->office;
    }

    // --------------------------------------------------------------

    /**
     * Set the office
     *
     * @param string $office
     */
    public function setOffice($office)
    {
        $this->office = $office;
    }

    // --------------------------------------------------------------

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    // --------------------------------------------------------------

    /**
     * Set phone
     *
     * Split the phone up and put it back together in a new format
     * before set it, per the assignment instructions
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        //There are literally a thousand ways to do this.
        //This is just one, and it probably isn't even the best one
        $pieceOne = substr($phone, 1, 3);
        $pieceTwo = substr($phone, 6, 3);
        $pieceThr = substr($phone, 10, 4);

        //Concatenate 
        $this->phone = $pieceOne . '-' . $pieceTwo . '-' . $pieceThr;
    }    

    // --------------------------------------------------------------

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    // --------------------------------------------------------------

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}

/* EOF: Person.php */