<?php
require('person.php');

//connect to db
$mysql_connection = mysqli_connect('2011.ispace.ci.fsu.edu', 'gh11e', 'w5jx3nyr', 'gh11e_lis4368');

//get db data
$dbquery = "SELECT firstName, lastName, title, office, phone, email FROM gh11e_lis4368.table1";

$dbdata = mysqli_query($mysql_connection, $dbquery);

$newPeople = array();
foreach($newPeople as $person) {

    $obj = new Person($person['firstName' . 'lastName']);
   
    $obj->setTitle($person['title']);
    $obj->setOffice($person['office']);
    $obj->setPhone($person['phone']);
    $obj->setEmail($person['email']);

    $newPeople[] = $obj;
    
}

$people = $newPeople;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title>LIS4368 | Fall 2012 | Assignment 3</title>

    <style type='text/css'>

        th {
            border-bottom: 1px solid #000;
        }

        td {
            border-bottom: 1px solid #999;
            padding: 4px;
        }
    </style>

</head>
<body>

<h1>XYZ Organization!</h1>

<table>
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Title</th>
            <th>Office</th>
            <th>Phone Number</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        <?php
        while ($user = mysqli_fetch_assoc($dbdata)){
            build_row($user);
        }

            foreach ($people as $person) {

                //One row per person..
                echo "<tr>";

                //Wrap the email in a <a href='...'> to make it a link
                echo "<td><a href='mailto:" . $dbquery("SELECT email FROM gh11e_lis4368") .
                        "'>" . $dbquery("SELECT email FROM gh11e_lis4368") .  "</a></td>";

                echo "</tr>";

            }
            /**
             * function to prevent reptition
             * @param array $user
             */
            function build_row($user){
                echo "<tr>";
                foreach ($user as $value){
                    build_cell($value);
                    
                }
                echo "</tr>";
            }
            /**
             * build_cell description
             * @param type $data
             */
            function build_cell($data){
                echo "<td>" . $data . '</td>';
            }

        ?>
    </tbody>
</table>

</body>
</html>