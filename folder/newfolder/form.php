<?php

require('person.php');
//require('assignment.php');

if (isset($_POST['firstName'], $_POST['lastName'], $_POST['title'], $_POST['office'], $_POST['phone'], $_POST['email'])) {
    $errors = array(); //Validate if error in field
    //arrary that is with link to the forms
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $title = $_POST['title'];
    $office = $_POST['office'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    
    if (strlen($firstName) === 0) {
        $errors[] = "Invalid first name";
    }
    
    if (strlen($lastName) === 0) {
        $errors[] = "Invalid last name";
    }
    
    if (strlen($office) === 0) {
        $errors[] = "Invalid office";
    }
    
    if (strlen($title) === 0) {
        $errors[] = "Invalid title";
    }
    
    if (strlen($phone) === 0) {
        $errors[] = "Invalid phone number";
    }
    
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errors[] = "Invalid email";
      }
      //This should be a foreach loop, but I am having difficulties with it
      
    if (!empty($errors)) {
        //loop through errors, and display them
        foreach ($errors as $error){
            echo '<strong>', $error, '</strong></p>';
          }
        } else {
            echo 'Valid input';
         }
 }
 

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Form</title>
    </head>
    <body>
        <form action="form.php" method="POST">
            First Name: <input type="text" name="firstName" /></p>
            Last Name:  <input type="text" name="lastName" /></p>
            Title:      <input type="text" name="title" /></p>
            Office:     <input type="text" name="office" /></p>
            Phone:      <input type="text" name="phone" /></p>
            Email:      <input type="text" name="email" /></p>
        <input type="submit" value="Submit" /></p>
        </form>
        <?php
        function isValid($firstName, $lastnNme, $title, $office, $phone, $email) {
            return !preg_match('/[A-Za-z]/', $firstName, $lastName, $title);
            return !preg_match('/[A-Za-z0-9 ]/', $office);
            return !preg_match('/[0-9()-]/', $phone);
            return !preg_match('/[A-Za-z@.]/', $email);
        }
        //Same as this, this should be a foreach loop
        //refilling the values
        function refillField($field){
            if(isset($_POST[$field])){
                return $_POST[$field];
            }
            else {
                return '';
            }
        }
        ?>
    </body>
</html>
