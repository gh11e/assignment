<?php

$dbConnection = new mysqli('2011.ispace.ci.fsu.edu', 'gh11e', 'w5jx3nyr', 'gh11e_lis4368');
if ($dbConnection->connect_error === null) {
  
  //Everything worked... Keep going..
  $result = $dbConnection->query("SELECT * FROM students;");
  $rows = $result->fetch_all(MYSQLI_ASSOC);
 
}
else {
  die("The connection to the database server failed.  Aborting...");
}

?>
<html>
  <head>
    <title>Students!</title>
  </head>
  <body>
    
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>FirstName</th>
          <th>LastName</th>
        </tr>
      </thead>
      <tbody>
        
         <?php 
          foreach($rows as $stud)
          {
            echo "<tr>";
            
            echo "<td>" . $stud['id'] . "</td>";
            echo "<td>" . $stud['firstName'] . "</td>";
            echo "<td>" . $stud['lastName'] . "</td>";
            
            echo "</tr>";
          }
         ?>
           
        
      </tbody>
    </table>
    
  </body>
</html>