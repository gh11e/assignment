<?php
require('person.php');

$mysqli_connection = mysqli_connect('2011.ispace.ci.fsu.edu', 'gh11e', 'w5jx3nyr', 'gh11e_lis4368');

$dbquery = "SELECT firstName, lastName, title, office, phone, email FROM gh11e_lis4368.table1";

$dbdata = mysqli_query($mysqli_connection, $dbquery);
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="description" content="LIS4368 Fall 2012 Assignment 2 Solution" />
    <meta name="author" content="Casey McLaughlin" />

    <title>LIS4368 | Fall 2012 | Assignment 2</title>

    <style type='text/css'>

        th {
            border-bottom: 1px solid #000;
        }

        td {
            border-bottom: 1px solid #999;
            padding: 4px;
        }
    </style>

</head>
<body>

<h1>XYZ Organization!</h1>

<table>
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Title</th>
            <th>Office</th>
            <th>Phone Number</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        
<?php

while ($user = mysqli_fetch_assoc($dbdata)){
    build_row($user);
}
echo "</table>";


function build_row($user){
    echo "<tr>";
    foreach ($user as $value) {
        build_cell($value);
    }
    echo "</tr>";
}

function build_cell($data){
    echo "<td>" . $data . "</td>";
}

?>