<?php

class Person
{
    //Here we create two different variables: one for first name
    //and another for last name.  This will make retrieval easier
    private $firstName;
    private $lastName;

    private $title;
    private $office;
    private $phone;
    private $email;
    private $username;
    private $password;


    /**
     * Constructor
     *
     * Changed this so that it does not accept any parameters, per the
     * instructions.  I could have also just deleted the constructor.
     *
     * @param string $name
     */
    public function __construct()
    {
        //nuthin..
    }

    // --------------------------------------------------------------

    /**
     * Get the name
     *
     * Our application expects the name to be in 'firstName lastName'
     * format, so we will concatenate here.
     *
     * @return string
     */
    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    // --------------------------------------------------------------

    /**
     * Set the name
     *
     * Here, we will use the explode() string function to break the
     * name up into first and lastname for storage.
     *
     * @param string $name
     */
    public function setName($name)
    {
        //Break up the name using explode() -- See php.net
        list($lname, $fname) = explode(',', $name, 2);

        //Set the class properties.  The firstName and lastName
        //will have extra spaces after running explode(), so trim them
        $this->firstName = trim($fname);
        $this->lastName  = trim($lname);
    }


    // --------------------------------------------------------------

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    // --------------------------------------------------------------

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    // --------------------------------------------------------------

    /**
     * Get office
     *
     * @return string
     */
    public function getOffice()
    {
        return $this->office;
    }

    // --------------------------------------------------------------

    /**
     * Set the office
     *
     * @param string $office
     */
    public function setOffice($office)
    {
        $this->office = $office;
    }

    // --------------------------------------------------------------

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    // --------------------------------------------------------------

    /**
     * Set phone
     *
     * Split the phone up and put it back together in a new format
     * before set it, per the assignment instructions
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        //There are literally a thousand ways to do this.
        //This is just one, and it probably isn't even the best one
        $pieceOne = substr($phone, 1, 3);
        $pieceTwo = substr($phone, 6, 3);
        $pieceThr = substr($phone, 10, 4);

        //Concatenate 
        $this->phone = $pieceOne . '-' . $pieceTwo . '-' . $pieceThr;
    }    

    // --------------------------------------------------------------

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    // --------------------------------------------------------------

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    // --------------------------------------------------------------
    /**
     * set and get username
     * @param string $username
     */
    public function getUsername()
    {
        return $this->username;    
    }
    
    public function setUsername($username)
    {
        $this->username = $username;
    }
    
    // --------------------------------------------------------------
    /**
     * set and get password
     * @param string $password
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    public function setPassword($password)
    {
        $this->password = $password;
    }
    
    // --------------------------------------------------------------

    /**
     * Save this Person to the database
     * 
     * @return boolean  TRUE if succeeded
     * @throws Exception
     */
    public function save()
    {
        //Establish a connection.  You would use your iSpace db here
        $dbConn = new mysqli('2011.ispace.ci.fsu.edu', 'gh11e', 'w5jx3nyr', 'gh11e_lis4368');
        
        //If there was no connect error, then let's build a query
        if ($dbConn->connect_error == null) {
            
            //I am using the built-in sprintf function to build my query,
            //because it is easier to read than using concatentation
            $query = sprintf(
                "INSERT INTO employees (firstName, lastName, title, office, phone, email, username, password) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
                $this->firstName, $this->lastName, $this->getTitle(), $this->getOffice(), $this->getPhone(), $this->getEmail()
            ); 
            
            //Result will be TRUE if it succeeded, or FALSE if it didn't
            //Refer to the mysqli::query() documentation on php.net
            $result = $dbConn->query($query);
            
            //If the query succeeds, return true.  If anything goes wrong, 
            //throw an Exception.
            if ($result === true) {
                return true;
            }
//            else {
//                throw new Exception("Error inserting data into database!");
//            }
        }
        else { //If connect error, then throw exception
            throw new Exception("Could not connect to the database!");
        }
    }
}

/* EOF: Person.php */