<?php
    //Need to require the Person.php file
    require('person.php');

    /**
     * Refill value if it exists in the POST array
     *
     * @param string $fieldName
     */
    function refill($fieldName)
    {
        if (isset($_POST[$fieldName])) {
            echo $_POST[$fieldName];
        }
        else {
            echo '';
        }
    }

    /**
     * Check to ensure a field contains only valid characters
     * 
     * @param string $fieldName
     * @param array $validCharacters 
     * @return boolean
     */
    function checkValidChracters($fieldName, $validCharacters)
    {
        //Get the fieldvalue from the POST array
        $fieldValue = $_POST[$fieldName];
        
        //Check each character; if we find a bad one, return false
        for($i = 0; $i < strlen($fieldValue); $i++) {
            
           //strpos() is a built-in PHP function -- look up how it works
           //{$i} means the character at index $i in the string $fieldValue
           if (strpos($validCharacters, $fieldValue{$i}) === false) {
               return false;
           }
        }
        
        //If made it here, return true
        return true;
    }
    
    // --------------------------------------------------------------
    
    //Check if the form was submitted, and process it.
    //
    //Could have also used:  if (count($_POST) > 0)...
    //
    if (isset($_POST['firstName']) && isset($_POST['lastName'])) {
        
        //
        //Validate!
        //
        
        //Setup an error array to hold error messages
        $errorMessages = array();
      
        //These are the fields we expect.  They are all required
        $expectedFields = array('firstName', 'lastName', 'title', 'office', 'phone', 'email', 'usernanme', 'password');
        
        //Check each one
        foreach($expectedFields as $eField) {
            
            //If the field isn't set, or it is empty, add an error message
            if ( ! isset($_POST[$eField]) OR $_POST[$eField] == '') {
                $errorMessages[] = "The " . $eField . " field is required!";
            }
        }
        
        
        //Here is a string of valid name characters
        $validNameChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789- ";
        
        //Ensure the name fields contain only valid characters        
        if (checkValidChracters('firstName', $validNameChars) === false) {
            $errorMessages[] = "First name has invlaid characters";
        }
        
        if (checkValidChracters('lastName', $validNameChars) === false) {
            $errorMessages[] = "Last name has invlaid characters";
        }
        
        //Ensure the office field contains only letters, numbers, and spaces
        //(same as names)
        if (checkValidChracters('lastName', $validNameChars) === false) {
            $errorMessages[] = "Last name has invlaid characters";
        }       
        
        //Ensure the phone number field only contains valid characters
        $validPhoneChars = "0123456789 -()";
        if (checkValidChracters('phone', $validPhoneChars) === false) {
            $errorMessages[] = "Phone has invlaid characters";
        }
        
        //Validating both username and password
        if (checkValidChracters('username', $validNameChars) === false) {
            $errorMessages[] = "Username has invlaid characters";
        }    
            
        if (checkValidChracters('password', $validNameChars) === false) {
            $errorMessages[] = "Password has invlaid characters";
        }        
        //
        // Check Validation and do actions with the form data!
        //
        
        //If no error messages, then there were no validation errors
        if (count($errorMessages) == 0) {
            
            //Create a new person object
            $person = new Person();
            $person->setName($_POST['lastName'] . ", " . $_POST['firstName']);
            $person->setPhone($_POST['phone']);
            $person->setOffice($_POST['office']);
            $person->setTitle($_POST['title']);
            $person->setEmail($_POST['email']);
            $person->setUsername($_POST['username']);
            $person->setPassword($_POST['password']); 
            //saving username and password to db
            $result = $person->save();
            
            if ($result === true) {
                $successMessage = "Added you to the database!";
            }
        }
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LIS4368 | Fall 2012 | Assignment 5</title>
    </head>
    <body>

        <h1>XYZ Organization!</h1>      
        
        <?php
            //If there was a success message...
            if (isset($successMessage)) {
                echo "<p class='success'>" . $successMessage . "</p>";
            }
            else { //print the form again .. notice how I open the bracket here...
        ?>

        <?php 
            //If there are error messages, print them
            if (isset($errorMessages) && count($errorMessages) > 0) {
                
                //Go through the array and print each message
                foreach($errorMessages as $msg) {
                    echo "<p class='error'>" . $msg . "</p>";
                }
            }
        ?>        
        
        <form action='form.php' method='post'>
            
            <fieldset>
                <legend>Your Information</legend>
                
                <p>
                    <label for='firstName'>First Name</label>
                    <input type='text' name='firstName' id='firstName' value='<?php refill('firstName'); ?>' />
                </p>
                
                <p>
                    <label for='lastName'>Last Name</label>
                    <input type='text' name='lastName' id='lastName' value='<?php refill('lastName'); ?>' />
                </p>
                
                <p>
                    <label for='title'>Title</label>
                    <input type='text' name='title' id='title' value='<?php refill('title'); ?>' />
                </p>
                
                <p>
                    <label for='office'>Office</label>
                    <input type='text' name='office' id='office' value='<?php refill('office'); ?>' />
                </p>
                
                <p>
                    <label for='phone'>Phone</label>
                    <input type='text' name='phone' id='phone' value='<?php refill('phone'); ?>' />
                </p>
                
                <p>
                    <label for='email'>Email</label>
                    <input type='email' name='email' id='email' value='<?php refill('email'); ?>' />
                </p>
                
                <p>
                    <label for='username'>Username</label>
                    <input type='text' name='username' id='username' value='<?php refill('username'); ?>' />
                </p>
                
                <p>
                    <label for='password'>Password</label>
                    <input type='password' name='password' id='password' value='<?php refill('password'); ?>' />
                </p>
               
            </fieldset>
            
            <fieldset>
                <input type='submit' value='Submit!' />
            </fieldset>
            
            
        </form>
        
        <?php }  ?>
    </body>
</html>