<?php
//Include our Person class php file (still need to do this!)
require('person.php');

// ------------------------------------------------------------------

//Create an empty array to hold employees
$people = array();

//Run a query to get all the employees from the database
//You would use your iSpace database credentials here.
$conn = new mysqli('2011.ispace.ci.fsu.edu', 'gh11e', 'w5jx3nyr', 'gh11e_lis4368');

//The result is a mysqli_result object
$result = $conn->query("SELECT * FROM gh11e_lis4368.table1");

//Use the mysqli_result->fetchObject to load each employee
//into a Person object..
while ($dbrow = $result->fetch_object('Person')) {
    $people[] = $dbrow;
}


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title>LIS4368 | Fall 2012 | Assignment 5</title>

    <style type='text/css'>

        th {
            border-bottom: 1px solid #000;
        }

        td {
            border-bottom: 1px solid #999;
            padding: 4px;
        }
    </style>

</head>
<body>

<h1>XYZ Organization!</h1>

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th>Office</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Username</th>
            <th>Password</th>
        </tr>
    </thead>
    <tbody>
        <?php

            //Since we are iterating through an array, the best
            //loop to use here is a foreach loop
            foreach ($people as $person) {

                //One row per person..
                echo "<tr>";

                echo "<td>" . $person->getName() .  "</td>";
                echo "<td>" . $person->getTitle() .  "</td>";
                echo "<td>" . $person->getOffice() .  "</td>";
                echo "<td>" . $person->getPhone() .  "</td>";

                //Wrap the email in a <a href='...'> to make it a link
                echo "<td><a href='mailto:" . $person->getEmail() . "'>" . $person->getEmail() .  "</a></td>";
                echo "<td>" . $person->getUsername() . "</td>";
                echo "<td>" . $person->getPassword() . "</td>";

                echo "</tr>";

            }

        ?>
    </tbody>
</table>

</body>
</html>