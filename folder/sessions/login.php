<?php
session_start();

require('person.php');

// ------------------------------------------------------------------

/**
 * @param string $fieldName
 */
function refill($fieldName)
{
    if (isset($_POST[$fieldName])) {
        echo $_POST[$fieldName];
    }
    else {
        echo '';
    }
}

if (isset($_POST['username']) && isset($_POST['password'])) {
    
    $errorMessages = array();


    //Connect to db

    $dbConn = new mysqli('2011.ispace.ci.fsu.edu', 'gh11e', 'w5jx3nyr', 'gh11e_lis4368');

    $result = $dbConn->query("SELECT * FROM gh11e_lis4368.table1");

    if ($result->num_rows == 1) {

        //We indicate the user is logged in by creating the 'username'
        //key in the SESSION array
        $_SESSION['username'] = $_POST['username'];

        //Also show a message
        $successMessage = "You have been logged-in!";

    }
    else {
        $errorMessages[] = "Invalid username/password.";
    }

}

// ------------------------------------------------------------------
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sessions</title>
    </head>
    <body>

        <h1>XYZ Organization!</h1>       
        
        <?php
            //If there was a success message...
            if (isset($successMessage)) {
                echo "<p class='success'>" . $successMessage . "</p>";
            }
            else { //print the form again .. notice how I open the bracket here...
        ?>

        <?php 
            //If there are error messages, print them
            if (isset($errorMessages) && count($errorMessages) > 0) {
                
                //Go through the array and print each message
                foreach($errorMessages as $message) {
                    echo "<p class='error'>" . $message . "</p>";
                }
            }
        ?>        
        
        <form action='login.php' method='post'>
            
            <fieldset>
                <legend>Login</legend>
                <p>
                    <label for='username'>Username</label>
                    <input type='text' name='username' id='username' value='<?php refill('username'); ?>' />
                </p>
               
                <p>
                    <label for='password'>Password</label>
                    <input type='password' name='password' id='password' value='' />
                </p>          
               
            </fieldset>
            
            <fieldset>
                <input type='submit' value='Submit!' />
            </fieldset>
            
            
        </form>
        
        <?php }?>
    </body>
</html>