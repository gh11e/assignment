<?php

$dbConnection = new mysqli('localhost', 'root', '', 'lis4368');
//$dbConnection = new mysqli('ispace.ci.fsu.edu', 'gh11e', 'password', 'gh11e_lis4368);
if ($dbConnection->connect_error === null) {
  
  //Course with ID of 2 is LIS1234
  $queryResult = $dbConnection->query("SELECT * FROM courses WHERE number='LIS1234'");
  $courseInfo = $queryResult->fetch_row();
    
  $queryResult = $dbConnection->query("SELECT * FROM join_students_courses WHERE courses_id = '2'");
  $qResult = $queryResult->fetch_all(MYSQLI_ASSOC);
  
  $students = array();
  
  //Get students from the database
  foreach($qResult as $row) {
    $info = $dbConnection->query("SELECT * FROM students WHERE id='" . $row['students_id'] . "'");
    $students[] = $info->fetch_object('Student');
    
    
    //Convert with student into an object
  }
   
}
else {
  die("The connection to the database server failed.  Aborting...");
}

?>
<html>
  <body>
    <h1>Info about the course</h1>
    <ul>
      <li>Course Name: <strong><?php echo $courseInfo[1]; ?></strong></li>
      <li>Course Num: <strong><?php echo $courseInfo[2]; ?></strong></li>
    </ul>
    
    <h2>Student list</h2>
    
    <ul>
    <?php 
      foreach ($students as $stu) {
        echo "<li>" . $stu[1] . " " . $stu[2] . "</li>";
      }
    ?>
    </ul>
    
  </body>
</html>