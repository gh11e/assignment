<?php
  session_start();
  require("functions.php");
  
  //Get list of courses
  $qresult = doQuery("SELECT * FROM courses");
  
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>List of Courses</title>
    
    <style type='text/css'>
      td {
        border: 1px solid #999;
      }
    </style>
  </head>
  <body>
    
    <?php include('header.php'); ?>
    
    <h1>List of courses!</h1>
    
    <table>
      <tbody>
          
           <?php
            
            while ($row = $qresult->fetch_row()) {
              echo "<tr>";
              echo "<td>" . $row[2] . "</td>";
              echo "<td>" . $row[1] . "</td>";
              echo "<td>" . $row[4] . "</td>";
              echo "</tr>";
            }
            ?>
         
      </tbody>
    </table>
    
    
  </body>
</html>