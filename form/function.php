<?php

/**
 * Do a query against the database
 * 
 * @param string $queryString     Your SQL Query to run
 * @return boolean|mysqli_result  Returns true for INSERT, UPDATE, DELETE
 * @throws Exception              If the connection failed
 */
function doQuery($queryString)
{
  //Connect to the database
  $dbConnection = new mysqli('localhost', 'root', '', 'lis4368');  
  
  //If failed, do an error
  if ($dbConnection->connect_error != null) {
    throw new Exception("There was an error connecting to the database!");
  }  
  
  //Run a query
  $qresult = $dbConnection->query($queryString);  
  
  //Return the results of the query
  return $qresult;
}

/**
 *  Checks to see if the user has already logged in
 * 
 *  If not, it redirects them to the login.php page
 */
function checkLoggedIn()
{
  //Check to see if user is logged-in
  if ( ! isset($_SESSION['isLoggedIn'])) {
    header('Location: login.php');
  }  
}

/**
 * Checks to see if a value contains expected characters
 *
 * @param string $whichField
 */
function checkCorrectCharacters($whichField)
{  
  $acceptableLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890- .";         
  for ($i = 0; $i < strlen($_POST[$whichField]); $i++) {

    //Get the character at the position $i and assign it to $currChar
    $currChar = $_POST[$whichField]{$i};

    if (strpos($acceptableLetters, $currChar) === false) {
      return "Your first name contains invalid characters!";
    }          
  }

  //if made it here
  return true;
}

/**
 * Checks to see if a variable contains only digits
 * 
 * @param string $whichField
 * @param int $requiredLength
 */
function checkNumbers($whichField, $requiredLength = 16)
{
  if ( ! is_numeric($_POST[$whichField]) OR strpos($_POST[$whichField], '.')) {
    return "The credit card must be only digits!";
  }

  //String should be 16 digits long
  if (strlen($_POST[$whichField]) != $requiredLength) {
    return "The credit card number is the wrong length!";
  }    

  //If made it here, we're good
  return true;
}

/**
 * Refills a field in a form automatically
 * 
 * @param string $whichField  The name of the form field
 * @return string  
 */
function refillField($whichField)
{
  if (isset($_POST[$whichField])) {
    return $_POST[$whichField];
  }
  else {
    return '';
  }
}


/* EOF */