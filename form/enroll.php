<?php
  session_start();
  require("functions.php");
  
  checkLoggedIn();
 
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
  </head>
  <body>
    
    <?php include('header.php'); ?>
    
    <h1>Enroll in our University</h1>
    
    <?php
    
      if (count($_POST) > 0) {
        
        //Validate the inputs
        $errorMessages = array();
        
        //Validate the firstName and lastName
        $result = checkCorrectCharacters('firstName');
        if ($result !== true) {
          $errorMessages[] = $result;
        }
        
        $result = checkCorrectCharacters('lastName');
        if ($result !== true) {
          $errorMessages[] = $result;
        }   
        
        //Validate the creditCard
        $result = checkNumbers('creditCard');
        if ($result !== true) {
          $errorMessages[] = $result;
        }
        
        //Check to see if anything failed
        if (count($errorMessages) == 0) {
          
          //everything worked.  Do whatever we need to with the data.
          $result = doQuery(sprintf("INSERT INTO STUDENTS (firstName, lastName, gradeLevel)
                            VALUES ('%s', '%s', '1'", $_POST['firstName'], $_POST['lastName']));
          
          $result = $dbConnection->query($query);
          
          if ($result === true) {
            echo "<h2>Congrats!  You are now enrolled!</h2>";
          }
          else {
            echo "<p>MAJOR SYSTEM ERROR.  Call and yell at your administrator!</p>";
          }
          
        }
        else { //Else, reprint the form along with some error messages
          
          echo "<h2>There were errors</h2>";
          
          foreach($errorMessages as $msg) {
            echo "<p>" . $msg . "</p>";
          }
        }
      }
    ?>
    
    <form method="post">
      
      <p>
        <label>First Name</label>
        <input name='firstName' value='<?php echo refillField('firstName'); ?>' />

        <label>Last Name</label>
        <input name='lastName' value='<?php echo refillField('lastName'); ?>' />
      </p>
      
      <p>
        <label>Credit Card Number</label>
        <input name='creditCard' value='<?php echo refillField('creditCard'); ?>' />
      </p>
      
      <p>
        <button type="submit">Enroll Me!</button>
      </p>
      
    </form>
    
  </body>
</html>