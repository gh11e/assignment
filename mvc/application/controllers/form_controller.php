<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form_controller extends CI_Controller {
	
	public function Form()
        {
               // Loading the model
            
               $this ->load->helper('url');        
               $this ->load->view('home');
        }
	
	public function index()
	{
		// Load the library
            
		$this->load->library(array('encrypt', 'form_validation', 'session'));
		
		// Load the helpers
		$this->load->helper(array('form', 'url'));
		
		// Set validation
		$this->form_validation->set_rules('user_name', 'username', 'required');
		$this->form_validation->set_rules('user_pass', 'password', 'required');
		//$this->form_validation->set_error_delimiters('<em>','</em>');
		
		// form been submitted and valid form info
		if($this->input->post('login'))
		{
			if($this->form_validation->run())
			{
				$user_name = $this->input->post('user_name');
				$user_pass = $this->input->post('user_pass');
				
				if(array_key_exists($user_name, $user_credentials))
                                    {
					if($user_pass == $this->encrypt->decode($user_credentials[$user_name]['user_pass']))
					{
						// user has been logged in
					}
					else
					{
						$this->session->set_flashdata('message', 'Incorrect password.');
						redirect('form_controller/index/');
					}
                                    }
				else
				{
					$this->session->set_flashdata('message', 'A user does not exist for the username specified.');
					redirect('form_controller/index/');
				}
			}
		}
		
		$this->load->view('header');
		$this->load->view('login_form');
		$this->load->view('footer');
                $this->session->keep_flashdata('item');
	}
	
                                               }

// EoF
// Location: ./application/controllers/form_controller.php