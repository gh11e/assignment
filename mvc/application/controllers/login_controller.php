<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_controller extends CI_Controller {

    public function Login()
    {
               // Loading the model
            
               $this ->load->helper('url');        
               $this ->load->view('home'); 
    }

    public function index()
    {
       //Load Library
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        
        //Load Helpers
        $this->load->helper(array('form'));
    
            
    $this->load->view('header');
    $this->load->view('employee_form');
    $this->load->view('footer');    
    $this->session->keep_flashdata('item');
    }

}
// EoF
// Location: ./application/controllers/login_controller.php