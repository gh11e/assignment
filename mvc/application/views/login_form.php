<h1>Login Form</h1>

<?php if($this->session->flashdata('message')) : ?>
    <p><?=$this->session->flashdata('message')?></p>
<?php endif; ?>

<?=form_open('form_controller')?>
    <?=form_fieldset('Login Form')?>

        <div class="textfield">
            <?=form_label('username', 'user_name')?>
            <?=form_input('user_name')?>
        </div>

        <div class="textfield">
            <?=form_label('password', 'user_pass')?>
            <?=form_password('user_pass')?>
        </div>

        <div class="buttons">
            <?=form_submit('login', 'Login')?>
        </div>

    <?=form_fieldset_close()?>
<?=form_close();?>