<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Viewlib {

    public function __construct()
    {
    }
    
    public function loadview($viewname, $data = array())
    {
      $CI =& get_instance();
      $CI->load->helper('url');        
              
      $CI->load->view('header');
      $CI->load->view($viewname, $data);
      $CI->load->view('footer');
    }    
}

?>