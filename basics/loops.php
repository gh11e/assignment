<?php

//For loop
for ($x = 0; $x < 15; $x++)
{
    echo $x;
}

//While loop
$x = 0;
while ($x < 15)
{
    echo $x;
    //$x = $x + 1; same as below
    $x++; //Same as VB x += 1
}
//do...While
$x = 0;
do {
    echo $x;
    $x++;    
} While($x < 15);


//Foreach loop
?>

