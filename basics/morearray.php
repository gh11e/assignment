<?php

//Associative Array
$assoArray = array();
$assoArray['Bob'] = "Jones";
$assoArray['Joe'] = "Smith";

foreach($assoArray as $firstName => $lastName) {
    echo "<br />Name:" . $firstName . " " . $lastName;
    
};

$myAssocWithValues['yetanother'] - "something";


//Multidimensional Array
$multArray = array();
$multArray[] = array('Bob', 'Jones');
$multArray[] = array('Stacy', 'Smith');

foreach($multArray as $person) {
    echo $person;
}

?>
