<?php

  //Incremental Array
  $numberList = array();
  $numberList[] = 'John';
  $numberList[] = 'Bob';
  $numberList[] = 'Betsey';
  $numberList[] = 'Randy';
  $numberList[] = 'Joe';
  $numberList[] = 'Kristen';
  $numberList[] = 'Stacy';
  
//  for ($i = 5; $i < count($numberList); $i = $i + 5) {   
//    //Concatenation (with the . operator)
//    echo 'The value is: ' . $numberList[$i];
//  }

  foreach($numberList as $employeeId => $name) {
     echo "<br>The item at " . $employeeId  ." is " . $name;
  }
  
  //Associative Array
  $assocArray = array();
  $assocArray['Bob'] = "Jones";
  $assocArray['Joe'] = "Smith";
  $assocArray['Rob'] = "Jones";
  $assocArray['Rob'] = "Robberson";
  
  foreach($assocArray as $firstName => $lastName) {
    echo "<br />Name:" . $firstName . " " . $lastName;
  }

  $myArrayWithValues = array('5', '20', 'Jeffery');
  
  $myAssocWithValues = array(
      'first'   => 'firstValue',
      'second'  => 'secValue',
      'another' => 'More Stuff'
  );
  
  //Later...
  $myAssocWithValues['yetanother'] = "something";
  
  //Multidimensional Array
  $multArray = array();
  $multArray[] = array('fname' => 'Bob', 'lname' => 'Jones');
  $multArray[] = array('fname' => 'Stacy', 'lname' => 'Smith');
  $multArray[] = array('fname' => 'Anna',  'lname' => 'Ann');
  
  foreach($multArray as $person) {  
    foreach($person as $whichName => $value) {
      echo "<br />" . $whichName . " --> " . $value;
    }  
  }
  
  foreach($multArray as $person) {
    echo "<br />" . $person['fname'] . ' ' . $person['lname'];
  }
  
?>