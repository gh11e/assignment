<?php

class Course
{
  private $name;
  private $number;
  private $description;
  private $times;
  private $roster = array();
          
  /**
   * Construct a new Course Object
   * 
   * A course object is used for representing
   * courses in our registration system
   * 
   * @param string $number  The course number (e.g. LIS3601)
   * @param string $name    The course name (e.g. "Web Development")
   */
  public function __construct($number, $name)
  {
    $this->setNumber($number);
    $this->setName($name);
  }
  
  /**
   * Add a student to the roster for this course
   * 
   * @param Student $student  A student object to add
   */
  public function addStudentToCourse(Student $student)
  {
    $this->roster[] = $student;
  }
  
  /**
   * Get the roster for this course
   * 
   * @return array
   */
  public function getRoster()
  {
    return $this->roster;
  }
  
  //Interface Methods
  public function setName($name)
  {
    $this->name = $name;
  }
  
  public function getName()
  {
    return $this->name;
  }
  
  public function setNumber($number)
  {
    $this->number = $number;
  }
  
  public function getNumber()
  {
    return $this->number;
  }
  
  public function setDescription($desc)
  {
    $this->description = $desc;
  }
  
  public function getDescription()
  {
    return $this->description;
  }
  
  public function setTimes($times)
  {
    $this->times = $times;
  }
  
  public function getTimes()
  {
    return $this->times;
  }
}


/* EOF */