<?php

class Student
{
  //Properties
  private $idNumber;
  private $firstName;
  private $lastName;
  private $gradeLevel;
  private $major;
  
  //Constructor Method
  public function __construct($id, $firstName, $lastName)
  {
    $this->setIdNumber($id);
    $this->setName($firstName, $lastName);
  }
  
  //Interface Methods
  private function setIdNumber($idNumber)
  {
    if (is_int($idNumber)) {
      $this->idNumber = $idNumber;
    }
    else {
      throw new InvalidArgumentException("The ID Number must be an integer!");
    }
  }
  
  public function getIdNumber()
  {
    return $this->idNumber;
  }
  
  public function setName($firstName, $lastName)
  {
    if ($firstName == 'John') {
      //throw new InvalidArgumentException("JOHN IS NOT ALLOWED!");
    }
    
    $this->firstName = $firstName;
    $this->lastName = $lastName;
  }
  
  public function getFirstName()
  {
    return $this->firstName;
  }
  
  public function getLastName()
  {
    return $this->lastName;
  }

  
  //Methods
  function attendCourse($whichCourse)
  {
    return "<p>I am attending " . $whichCourse . "</p>";
  }
  
}

/* EOF */