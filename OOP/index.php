<?php
  //Load some files
  require('Student.php');
  //require('GradStudent.php');
  require('Course.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
  </head>
  <body>
    <h1>Course Registration!</h1>
    <?php
    
      //Insantiate a new Course Object
      $myCourse = new Course('LIS4368', 'PHP and Stuff');
    
      //Create a list of students
      $myListOfStudents = array();
      $myListOfStudents[] = new Student(123, 'Mary', 'Smith');
      $myListOfStudents[] = new Student(555, 'John', 'Patterson');
      $myListOfStudents[] = new Student(765, 'Mike', 'McDonald');
      $myListOfStudents[] = new Student(234, 'Casey', 'McL');
      $myListOfStudents[] = new Student(999, 'Joe', 'Schmuck');
      
      //Foreach student, add him or her to the course
      foreach($myListOfStudents as $student) {
        $myCourse->addStudentToCourse($student);
      }
             
      //Print all the students in my course
      echo "<h2>Students in " . $myCourse->getName() . "</h2>";
      echo "<ol>";
      foreach($myCourse->getRoster() as $studentInCourse) {
        echo "<li>" . $studentInCourse->getLastName() . ', ' . $studentInCourse->getFirstName() . "</li>";
      }
      echo "</ol>";
      
      //Grad student?
//      $myGradStudent = new GradStudent(2222, 'Billy', "Bob");
//      echo "<p>GRAD STUDENT: " . $myGradStudent->getFirstName() . "</p>";
      
    ?>
  </body>
</html>
