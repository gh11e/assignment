<?php
  //Load some files
  require('Stu.php');
  require('Cour.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
    </style>
    </title>
  </head>
  <body>
      
    <h1>XYZ Organization</h1>
    <table border="1" width="70%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th>Office</th>
            <th>Phone Number</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
    <?php
    
      //Insantiate a new Course Object
      $myCourse = new Course('','','','','');
    
      //Create a list of students
      $myListOfStudents = array();
      $myListOfStudents[] = new Student('Tom', 'Green', 'Computer Systems Control Specialist', '123 DSL', '850-555-1422','t.green@example.xyz.org');
      $myListOfStudents[] = new Student('Xiaoguang', 'Li', 'Systems Administrator', '456 DSL', '850-555-0188', 'x.li@example.xyz.org');
      $myListOfStudents[] = new Student('Edson', 'Manners', 'Applications Adminstrator', '789 DSL', '850-554-6490', 'e.manners@examples.xyz.org');
      $myListOfStudents[] = new Student('Justin', 'Marshall', 'Systems Adminstrator', '987 DSL', '850-554-5559', 'j.marshall@example.xyz.org');
      $myListOfStudents[] = new Student('Michael', 'McDonald', 'Assistant in Research / Systems Adminstrator', '654 DSL', '850-577-7032', 'm.mcdonald@example.xyz.org');
      
      //Foreach student, add him or her to the course
      foreach($myListOfStudents as $student) {
        $myCourse->addStudentToCourse($student);
      }
             
      //Print all the students in my course
      echo $myCourse->getName();
      echo $myCourse->getTitle();
      echo $myCourse->getOffice();
      echo $myCourse->getPhone();
      echo "<tr>";
      foreach($myCourse->getRoster() as $studentInCourse) {
          echo "<tr>";
        echo "<td>" . $studentInCourse->getFirstName() . " " . $studentInCourse->getLastName() . "</td>";
//        echo "<td>" . $studentInCourse->getLastName() . "</td>";
        echo "<td>" . $studentInCourse->getTitle() . "</td>";
        echo "<td>" . $studentInCourse->getOffice() . "</td>";
        echo "<td>" . $studentInCourse->getPhone() . "</td>";
        echo "<td><a href='mailto:" . $studentInCourse->getEmail() . "'>" . $studentInCourse->getEmail() . "</a></td>";
        echo "<tr>";
      }
      echo "</tr>";
    ?>
    </tbody>
    </table>
  </body>
</html>