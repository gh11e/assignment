<?php

class Course
{
  private $name;
  private $title;
  private $office;
  private $phone;
  private $email;
  private $roster = array();
  
  public function __construct($name, $title, $office, $phone, $email)
  {
    $this->setName($name);
    $this->setTitle($title);
    $this->setOffice($office);
    $this->setPhone($phone);
    $this->setEmail($email);
  }

  public function addStudentToCourse(Student $student)
  {
    $this->roster[] = $student;
  }

  public function getRoster()
  {
    return $this->roster;
  }
  
  //Interface Methods
  public function setName($name)
  {
    $this->name = $name;
  }
  
  public function getName()
  {
    return $this->name;
  }
  
  public function setTitle($title)
  {
    $this->title = $title;
  }
  
  public function getTitle()
  {
    return $this->title;
  }
  
  public function setOffice($office)
  {
    $this->office = $office;
  }
  
  public function getOffice()
  {
    return $this->office;
  }
  
  public function setPhone($phone)
  {
    $this->phone = $phone;
  }
  
  public function getPhone()
  {
    return $this->phone;
  }
  
  public function getEmail($email)
  {
      $this->email = $email;
      
  }
  
  public function setEmail()
  {
      return $this->email;
  }
  
}