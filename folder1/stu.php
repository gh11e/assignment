<?php

class Student
{
  //Properties
  private $firstName;
  private $lastName;
  private $title;
  private $office;
  private $phone;
  private $email;
  
  //Constructor Method
  public function __construct( $firstName, $lastName, $title, $office, $phone, $email)
  {
    $this->setName($firstName, $lastName);
    $this->setTitle($title);
    $this->setOffice($office);
    $this->setPhone($phone);
    $this->setEmail($email);
  }
  
  public function setName($firstName, $lastName)
  {
    
    $this->firstName = $firstName;
    $this->lastName = $lastName;
  }
  
  public function getFirstName()
  {
    return $this->firstName;
  }
  
  public function getLastName()
  {
    return $this->lastName;
  }
  
  public function setTitle($title)
  {
      $this->title = $title;
  }
  
  public function getTitle()
  {
      return $this->title;
  }

  public function setOffice($office)
  {
      $this->office = $office;
      
  }
  
  public function getOffice()
  {
      return $this->office;
  }
  
  public function setPhone($phone)
  {
      $this->phone = $phone;
  }
  
  public function getPhone()
  {
      return $this->phone;
      
  }  
  
  public function setEmail($email)
  {
      $this->email = $email;
  }
  
  public function getEmail()
  {
      return $this->email;
  }

}