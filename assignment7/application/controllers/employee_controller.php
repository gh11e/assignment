<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_controller extends CI_Controller {

    public function index()

    {

     //Loading the model
        
     
     $this ->load->helper('url');        
     $this ->load->view('home');   

    }
    
    public function directory() {
      
      //Loading the model

      $this->load->model('Employee_model');

      

      //Get the employee information from the model

      $personList = $this->Employee_model->get_Employee();
        

      //send the employee information to the view

      $data = array();

      $data['Employee'] = $personList;

        

      //Load up the view

      $this->load->view('employee_directory', $data);
    }

}


/* End of file my.php */