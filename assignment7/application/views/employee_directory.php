<!DOCTYPE html>
<html>
    <head>
        <style>
        table{
            border-width: 1px;
            border-color: #000000;
            border-style: solid;
        } 

       td{
            border-color: #000000;
            border-style: solid;
            border-width: 1px;
            padding: 1px;
        }
   
            
        </style>    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Assignment 6</title>
    </head>
    <body>
        
        <h1>XYZ Organization</h1>
           

        <table>
            
            
                
            
            
            <thead>

                <tr>

                    <th>Name</th>

                    <th>Title</th>
                    
                    <th>Office</th>
                    
                    <th>Phone Number</th>
                    
                    <th>Email</th>
                  

                </tr>

            </thead>

            <tbody>

                <?php 

               foreach ($Employee as $newPerson) {

                echo "<tr>";

                echo "<td>" . $newPerson['firstName'] . " " . $newPerson['lastName'] . "</td>";
                echo "<td>" . $newPerson['title'] .  "</td>";
                echo "<td>" . $newPerson['office'] .  "</td>";
                echo "<td>" . $newPerson['phone'] .  "</td>";
                echo "<td><a href='mailto:" . $newPerson['email'] . "'>" . $newPerson['email'] .  "</a></td>";

                echo "</tr>";

               }

                ?>              

            </tbody>

        </table>

       
    </body>
</html>